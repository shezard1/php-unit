function makeForm(data) {
    let formData = new FormData();
    for(let key in data) {
        formData.append(key, data[key]);
    }
    return formData;
}

function request(path, params, success, error) {
    fetch(path, params)
        .then(function(response) {
            return {
                'status': response.status,
                'promise': response.text()
            };
        })
        .then(function(response) {
            if(response.status === 200) {
                response.promise.then(success);
            } else {
                response.promise.then(error);
            }
        });
}

let submitPath = document.querySelector('#submit-path');

submitPath.addEventListener('click', function() {

    let path = document.querySelector('#path').value;
    let formData = makeForm({
        path: path
    });

    document.querySelector('#path-error').style.display = 'none';
    document.querySelector('#path-error').innerHTML = '';
    document.querySelector('#diff').innerHTML = '';
    document.querySelector('#branches').innerHTML = '';

    request('/?route=submit-path', {
        method: 'POST',
        body: formData,
    }, (body) => {
        document.querySelector('#branches').innerHTML = body;
        submit();
    }, () => {
        document.querySelector('#path-error').style.display = 'block';
        document.querySelector('#path-error').innerHTML = `${path} is not a git folder`
    });
});

function submit() {

    let button = document.querySelector('#submit');

    button.addEventListener('click', function() {

        let formData = makeForm({
            curl: document.querySelector('#curl').value,
            branch: document.querySelector('#branches-select').value,
            path: document.querySelector('#path').value
        });
    
        document.querySelector('#curl-error').style.display = 'none';
        document.querySelector('#curl-error').innerHTML = '';

        request('/?route=curl', {
            method: 'POST',
            body: formData,
        }, (body) => {
            document.querySelector('#diff').innerHTML = body;
        }, (body) => {
            document.querySelector('#curl-error').style.display = 'block';
            document.querySelector('#curl-error').innerHTML = body;
        });
    });
}