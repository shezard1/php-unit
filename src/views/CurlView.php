<?php
namespace cw\views;

use cw\lib\BaseView;

class CurlView extends BaseView
{
    public function render(array $data = [])
    {
        return $this->wrap('home/diff', [
            'content' => $data['diff']
        ]);
    }
}
