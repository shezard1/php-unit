<?php
namespace cw\views;

use cw\lib\BaseView;
use cw\lib\Tombstone;

class HomeView extends BaseView
{
    public function render(array $data = [])
    {
        new Tombstone('2019-11-27', 'First pass');
        return $this->wrap('home/home', [
            'path' => \Path::getRootDir()
        ]);
    }
}
