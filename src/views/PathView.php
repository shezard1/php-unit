<?php
namespace cw\views;

use cw\lib\BaseView;

class PathView extends BaseView
{
    public function render(array $data = [])
    {
        return $this->wrap('home/path', [
            'branches' => $data['branches']
        ]);
    }
}
