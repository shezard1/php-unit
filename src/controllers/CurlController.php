<?php
namespace cw\controllers;

use cw\lib\BaseController;
use cw\differ\Differ;

use Symfony\Component\HttpFoundation\Response;

class CurlController extends BaseController
{
    public function process() : Response
    {
        $curl = $this->request->get('curl');
        $branch = $this->request->get('branch');
        $path = $this->request->get('path');

        if (strpos($curl, 'curl ') !== 0) {
            return new Response(
                'Bad curl request',
                Response::HTTP_BAD_REQUEST,
                ['content-type' => 'text/html']
            );
        }

       
        $differ = new Differ($curl, $branch, $path);


        try {
            $content = $this->view->render([
                'diff' => $differ->html(),
            ]);
        } catch (\Exception $e) {
            return new Response(
                'Git can\'t switch branch',
                Response::HTTP_BAD_REQUEST,
                ['content-type' => 'text/html']
            );
        }

        return new Response(
            $content,
            Response::HTTP_OK,
            ['content-type' => 'text/html']
        );
    }
}
