<?php
namespace cw\controllers;

use cw\lib\BaseController;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends BaseController
{
    public function process() : Response
    {
        $content = $this->view->render();

        return new Response(
            $content,
            Response::HTTP_OK,
            ['content-type' => 'text/html']
        );
    }
}
