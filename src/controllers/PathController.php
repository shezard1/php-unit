<?php
namespace cw\controllers;

use cw\lib\BaseController;
use Symfony\Component\HttpFoundation\Response;

class PathController extends BaseController
{
    public function process() : Response
    {
        $path = $this->request->get('path');

        exec("cd $path && git branch -l", $branches, $error);

        if (empty($branches)) {
            return new Response(
                '',
                Response::HTTP_NOT_FOUND,
                ['content-type' => 'text/html']
            );
        }

        $branches = array_filter($branches, function ($branch) {
            return $branch[0] != '*';
        });

        $branches = array_map(function ($branch) {
            return trim($branch);
        }, $branches);

        $content = $this->view->render([
            'branches' => $branches,
        ]);

        return new Response(
            $content,
            Response::HTTP_OK,
            ['content-type' => 'text/html']
        );
    }
}
