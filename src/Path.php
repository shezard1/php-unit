<?php

class Path
{
    public static function getRootDir() : string
    {
        $path = realpath(__DIR__ . '/..');
        return $path ? $path : '';
    }

    public static function getSrcDir() : string
    {
        return __DIR__;
    }
}
