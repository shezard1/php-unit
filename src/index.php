<?php

require_once('vendor/autoload.php');

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use \cw\lib\Router;

$router = new Router();
$router->register('home', 'Home', 'Home');
$router->register('submit-path', 'Path', 'Path');
$router->register('curl', 'Curl', 'Curl');

$request = Request::createFromGlobals();

if ($request->getBasePath() == '/public') {
    $content = file_get_contents(__DIR__ . '/..' . $request->getRequestUri());
    if ($content === false) {
        $content = null;
    }
    $response = new Response(
        $content,
        Response::HTTP_OK,
        ['content-type' => 'text/javascript']
    );
    $response->send();
    exit();
}

$routeName = $request->query->get('route') ?? 'home';
$route = $router->get($routeName);

if ($route != null) {
    list($controllerName, $viewName) = $route;
    
    $ControllerClass = "\\cw\\controllers\\${controllerName}Controller";
    $ViewClass = "\\cw\\views\\${viewName}View";
    
    $controller = new $ControllerClass($request, $ViewClass);
    
    $response = $controller->process();
    
    if ($response instanceof Response) {
        $response->send();
    }
}

register_shutdown_function(function () {
    $url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $headers = [
        'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:71.0) Gecko/20100101 Firefox/71.0',
        'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3',
        'DNT: 1',
        'Connection: keep-alive',
        'Upgrade-Insecure-Requests: 1',
        'Pragma: no-cache',
        'Cache-Control: no-cache',
        'Cookie: XSRF-TOKEN=gmc583gq0fredpuc3p6tpquj12; JWT-SESSION=eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJBVzhJMW1fTlBKd0t2WGFHVXV1dCIsInN1YiI6IkFXOEkxYks4UEp3S3ZYYUdVc2sxIiwiaWF0IjoxNTc2NDAxMjY4LCJleHAiOjE1NzY2ODk1NTUsImxhc3RSZWZyZXNoVGltZSI6MTU3NjQwMTI2ODY3OCwieHNyZlRva2VuIjoiZ21jNTgzZ3EwZnJlZHB1YzNwNnRwcXVqMTIifQ.8MA4oQxTz1qLjl0T65BvWi9iijBT_v7IZ0QE0NNxHyo; io=BB9Vlbvb25LuosXdAAAB; PHPSESSID=1mc6ejqg0q634j3vu85im93at8',
    ];

    $cookies = [];
    foreach ($_COOKIE as $name => $value) {
        $cookies[] = "$name: $value";
    }

    $headers[] = 'Cookie: ' . implode('; ', $cookies);
    $record = 'curl "' . $url . '" -H "' . implode('" -H "', $headers) . '"';
    exec("echo $record >> log.txt");
});
