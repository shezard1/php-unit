<div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text">Branches</span>
    </div>
    <select id="branches-select" class="form-control">
        {% for branch in branches %}
            <option>{{ branch }}</option>
        {% endfor %}
    </select>
</div>

<div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text">@</span>
        </div>
        <input id="curl" type="text" class="form-control" placeholder="Curl Url">
    </div>

    <div id="curl-error" class="alert alert-danger" style="display:none;"></div>

    <button id="submit" type="button" class="btn btn-primary">Send</button>
    <br />
<div>