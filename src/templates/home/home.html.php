{% extends "wrapper.html.php" %}

{% block content %}
<div class="container-fluid">

    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">...</span>
        </div>
        <input id="path" type="text" class="form-control" placeholder="{{ path }}" value="{{ path }}">
    </div>

    <div id="path-error" class="alert alert-danger" style="display:none;"></div>

    <button id="submit-path" type="button" class="btn btn-primary">Test Path</button>
    <br />
    <br />

    <div id="branches"></div>

    <div id="diff"></div>
</div>
<div style="display:none;">A diff</div>
<script type="text/javascript" src="/public/send.js"></script>
{% endblock content %}