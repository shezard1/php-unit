<?php
namespace cw\tests;

use PHPUnit\Framework\TestCase;
use cw\lib\BaseController;
use Symfony\Component\HttpFoundation\Request;

final class BaseControllerTest extends TestCase
{
    public function testBaseControllerCanBeCreated()
    {
        $request = new Request();

        $baseController = new BaseController($request, '\\cw\\lib\\BaseView');

        $this->assertInstanceOf(
            BaseController::class,
            $baseController
        );
    }
}
