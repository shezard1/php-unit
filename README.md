Gitlab CI - phpunit
===================

Launching the server:
---------------------

`composer install`
`php -S localhost:12345 src/index.php`

CI/CD
-----

When remote receive a push:

1) Run phpunit, php-cs-fixer and php-stan
2) Run `ci/deploy.sh` if on the `master` branch and step 1) Succeed

To run step 1) locally:

`composer run-script analyse`
`composer run-script check`
`composer run-script tests`

Tombstone
---------

To analyse tombstone logs localy:

`composer run-script tombstone`

Resources:
----------

- Doc de gitlab-ci: https://docs.gitlab.com/ee/ci/examples/php.html#example-project
- Doc de PHPUnit: https://phpunit.de/getting-started/phpunit-8.html
- Doc de PHP-CS-FIXER: https://github.com/FriendsOfPHP/PHP-CS-Fixer/blob/2.16/README.rst
- Un bon article sur gitlab-ci: https://blog.eleven-labs.com/fr/introduction-gitlab-ci/
- Un fichier .gitlab-ci.yml complet: https://gitlab.cwd.at/symfony/cwd-standard-edition/blob/develop/.gitlab-ci.yml