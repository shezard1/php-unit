<?php
namespace cw\differ;

use Jfcherng\Diff\Differ as Diff;
use Jfcherng\Diff\DiffHelper;
use Jfcherng\Diff\Factory\RendererFactory;

class Differ
{
    private $_curl;
    private $_branch;
    private $_path;

    public function __construct(string $curl, string $branch, string $path)
    {
        $this->_curl = $curl;
        $this->_branch = $branch;
        $this->_path = $path;
    }

    private function _checkout($branch)
    {
        $error = 0;
        $path = $this->_path;
        exec("cd $path && git checkout $branch", $output, $error);
        if ($error) {
            throw new \Exception('Can\'t switch branch');
        }
    }

    private function _run($command)
    {
        $output = [];
        exec($command, $output);
        return $output;
    }

    private function _diff()
    {
        $this->_checkout($this->_branch);
        $output = $this->_run($this->_curl);

        $this->_checkout('-');
        $output2 = $this->_run($this->_curl);

        return [$output, $output2];
    }

    public function html()
    {
        $diffs = $this->_diff();

        $old = implode(PHP_EOL, $diffs[0]);
        $new = implode(PHP_EOL, $diffs[1]);

        return DiffHelper::calculate($old, $new, 'Inline');
    }
}
