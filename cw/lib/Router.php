<?php
namespace cw\lib;

class Router
{
    private $_routes = [];

    public function register(string $routeName, string $controllerName, string $viewName)
    {
        $this->_routes[$routeName] = [$controllerName, $viewName];
    }

    public function get(string $route)
    {
        if (isset($this->_routes[$route])) {
            return $this->_routes[$route];
        }
        return null;
    }
}
