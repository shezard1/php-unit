<?php

namespace cw\lib;

interface ViewInterface
{
    public function render(array $data = array());
}
