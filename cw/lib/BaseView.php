<?php
namespace cw\lib;

use Path;

class BaseView implements ViewInterface
{
    protected $twig = null;

    public function __construct()
    {
        $loader = new \Twig\Loader\FilesystemLoader(Path::getSrcDir() . '/templates/');
        $this->twig = new \Twig\Environment($loader);
    }

    protected function wrap(string $name, array $params = [])
    {
        return $this->twig->render("$name.html.php", $params);
    }

    public function render(array $data = [])
    {
    }
}
