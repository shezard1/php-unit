<?php
namespace cw\lib;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BaseController implements BaseControllerInterface
{
    protected $request = null;
    protected $view = null;

    public function __construct(Request $request, string $ViewClass)
    {
        $this->request = $request;
        $this->view = new $ViewClass();
    }

    public function process()
    {
    }
}
