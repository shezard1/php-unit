<?php
namespace cw\lib;

use Scheb\Tombstone\Graveyard;
use Scheb\Tombstone\Handler\AnalyzerLogHandler;
use Scheb\Tombstone\Handler\StreamHandler;

class Tombstone
{
    private $_logDir = 'graveyard';

    public function __construct(string ...$arguments)
    {
        $logDir = $this->_logDir;
        $streamHandler = new StreamHandler("$logDir/tombstones.log");
        $analyzerLogHandler = new AnalyzerLogHandler($logDir);

        $graveyard = new Graveyard();
        $graveyard->addHandler($streamHandler);
        $graveyard->addHandler($analyzerLogHandler);

        try {
            $trace = \Scheb\Tombstone\Tracing\TraceProvider::getTraceHere();
            $graveyard->tombstone($arguments, $trace, []);
        } catch (\Exception $e) {
        }
    }
}
