<?php
namespace cw\lib;

interface BaseControllerInterface
{
    public function process();
}
